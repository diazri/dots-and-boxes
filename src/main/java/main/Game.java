package main;

import org.apache.commons.cli.*;
import lib.*;
import search.MinMax;

import java.util.*;
import java.util.regex.*;

public class Game {

	// The defaults.
	public static final int DEFAULT_SIZE = 3;
	public static final int DEFAULT_PLYS = 2;

	// Instance variables.
	private final int plys, width, depth;

	// The board for our game.
	private Board board;

	/** A constructor.
	 * 
	 * @param p - the number of plys the AI will search.
	 * @param w - the width of the board.
	 * @param d - the depth of the board.
	 */
	public Game(int p, int w, int d) {
		plys = p;
		width = w;
		depth = d;

		board = new Board(width, depth);
	}

	/** Play a game.
	 * 
	 */
	public void play() {
		boolean playing = true;
		System.out.println("The game begins. The board is now:\n" + board);
		while(playing) {
			playing = playerTurn();
			playing = AITurn();
		}
		System.out.println("Game over.");
		if(board.scoreDif() > 0) System.out.println("AI wins!");
		else if(board.scoreDif() < 0) System.out.println("Player wins!");
		else System.out.println("It's a tie!");
	}
	
	/** Let the player go.
	 * 
	 * @return - false if the game is over, true otherwise.
	 */
	public boolean playerTurn() {
		// Ask the user for input.
		System.out.println("What line would you like to draw?");
		
		// Accept the user's input.
		Scanner sc = new Scanner(System.in);
		String s = sc.nextLine();
		
		// Make sure the user input was legal (int,int).
		Pattern regx = Pattern.compile("\\d+,\\d+");
		Matcher m = regx.matcher(s);
		
		// If the user input is ILLEGAL, ask them to try again.
		if(!m.matches()) {
			System.out.println("ERROR: Invalid input '" + s + "'.\n"
					+ "Input must match the regx 'd+,d+'!\n"
					+ "Please try again.");
			return playerTurn();
		}
		
		// Now that we've verified our input, we can continue on.
		
		// Get the space the player wants to fill in.
		String[] pair = s.split(",");
		int x = Integer.valueOf(pair[0]);
		int y = Integer.valueOf(pair[1]);
		
		// If BOTH or NEITHER x and/or why are even, we've got another error.
		if(! ((x%2 == 0) ^ (y%2 == 0)) ) {
			System.out.println("ERROR: Invalid input '" + s + "'.\n"
					+ "Either your x-coordinate or your y-coordinate (but not both) must be EVEN."
					+ "Please try again.");
			return playerTurn();
		}
		
		// If the player tries moving to a space with a line, we've got still another error.
		if(! board.isLegal(x, y) ) {
			System.out.println("ERROR: Invalid input '" + s + "'.\n"
					+ "You are trying to move to a space where a line has already been drawn."
					+ "Please try again.");
			return playerTurn();
		}
		
		// Move there.
		board = board.move(x, y);
		
		System.out.println("You made your move. The board is now:\n" + board);
		
		// If there are more moves to be made, return true.
		return board.succ().size() > 0;
	}
	
	/** Let the AI go.
	 * 
	 * @return - false if the game is over, true otherwise.
	 */
	public boolean AITurn() {
		// Let the AI pick it's move.
		board = MinMax.move(board, plys);
		
		// Print the AI's move.
		System.out.println("The AI made it's move. The board is now:\n" + board);
		
		// If there are more moves to be made, return true.
		return board.succ().size() > 0;
	}
	
	public static void main(String[] args) {
		// Our commandline object for handling arguments.
		CommandLine cl;

		// Define command line options
		Options ops = new Options();

		Option opt = Option.builder("p")
				.longOpt("plys")
				.hasArg()
				.desc("How many plys will the AI explore? Must be an integer.")
				.build();
		ops.addOption(opt);

		opt = Option.builder("w")
				.longOpt("width")
				.hasArg()
				.desc("How wide will the board be? Must be an integer.")
				.build();
		ops.addOption(opt);

		opt = Option.builder("d")
				.longOpt("depth")
				.hasArg()
				.desc("How deep will the board be? Must be an integer.")
				.build();
		ops.addOption(opt);

		ops.addOption("h", "help", false, "Show the help menu.");

		// Parse the command line options
		CommandLineParser par = new DefaultParser();
		try {
			cl = par.parse(ops, args);
		} catch (ParseException e) {
			System.out.println();
			e.printStackTrace();
			throw new IllegalArgumentException("ERROR: Invalid arguments.");
		}

		// Some defaults.
		int p = DEFAULT_PLYS, w = DEFAULT_SIZE, d = DEFAULT_SIZE;

		// If the user asked for help, print the help menu and return.
		if (cl.hasOption("h")) {
			HelpFormatter help = new HelpFormatter();
			help.printHelp("SearchApp", ops);
			return;
		}
		
		// If the user supplied args, use them.
		if (cl.hasOption("p")) {
			String s = cl.getOptionValue("p");
			p = Integer.valueOf(s);
		}
		
		if (cl.hasOption("w")) {
			String s = cl.getOptionValue("w");
			w = Integer.valueOf(s);
		}
		
		if (cl.hasOption("d")) {
			String s = cl.getOptionValue("d");
			d = Integer.valueOf(s);
		}
		
		// Build a game.
		Game g = new Game(p, w, d);
		g.play();
	}

}
