package search;

import java.util.LinkedList;

import static lib.Debug.dp;
import lib.*;

public class MinMax {

	// Our queue.
	private static LinkedList<Node> q;
	
	/** Compute the next move.
	 * 
	 * @param b - the board to work from.
	 * @param d - the depth to search to.
	 * @return - the next move.
	 */
	public static Board move(Board b, int d) {
		// Do a min-max search.
		Node root = search(b, d);
		
		// Pick the best move.
		return root.move();
	}
	
	/** Run a min-max search.
	 * 
	 * @param b - the board to begin from.
	 * @param d - the depth (number of plys) to search.
	 * @return - a the root of a search tree of depth d.
	 */
	public static Node search(Board b, int d) {
		// Initialize our node & our queue.
		Node root = new Node(b);
		q = new LinkedList<Node>();
		q.add(root);
		
		// Run the search.
		while(!q.isEmpty()) {
			// Pop a node.
			Node n = q.removeFirst();
			
			dp("Popped node : " + n);
			
			// If n is at or above our target depth,
			if(n.getDepth() <= d) {
				// Add all n's successors to our queue.
				// n will establish parent-child links internally.
				q.addAll(n.succ());
			}
		}
		
		return root;
	}
}
