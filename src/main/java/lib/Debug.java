package lib;

public class Debug {

	public static final boolean debug = false;
	
	/** Debug print. Prints only when in debug mode.
	 * 
	 * @param s - the string to print.
	 */
	public static void dp(String s) {
		if(debug) System.out.println(s);
	}

}
