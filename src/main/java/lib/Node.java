package lib;

import java.util.*;

public class Node {

	// Min or max?
	private static final boolean MIN = false;
	private static final boolean MAX = true;
	
	// What type of node is this?
	private final boolean type;
	
	// How deep is this node?
	private final int depth;
	
	// The board this node corresponds to.
	private final Board board;
	
	// Parent & child links.
	private Node parent;
	private List<Node> children;
	
	/** A constructor. Defaults to MAX.
	 * 
	 * @param b - the board this node represents.
	 */
	public Node(Board b) {
		board = b;
		type = MAX;
		depth = 0;
	}
	
	/** Another constructor, for specifying type.
	 * 
	 * @param b - the board this node represents.
	 * @param m - the type of this board (false for min, true for max).
	 */
	public Node(Board b, boolean m, int d) {
		board = b;
		type = m;
		depth = d;
	}
	
	/** Build this node's successors & add them as children.
	 * 
	 * @return - a list of this node's successors.
	 */
	public List<Node> succ() {
		List<Node> succs = new LinkedList<Node>();
		
		// For each successor to this node's board,
		for(Board b : board.succ()) {
			// Add a successor with that board, and the opposite type of this node.
			Node n = new Node(b, !type, this.depth + 1);
			n.parent = this;
			succs.add(n);
		}
		
		// Add the successors as children & return them.
		this.children = succs;
		return succs;
	}
	
	/** Based on this node's successors, compute the AI's next move.
	 * 
	 * @return - a board representing the AI's next move.
	 */
	public Board move() {
		// Error handling.
		if(this.type == MIN) throw new IllegalStateException("ERROR: Root node is a MIN node. Search trees should always have a MAX node as their root.");
		if(this.children == null || this.children.isEmpty()) throw new IllegalStateException("ERROR: This node has no children! The AI must explore past depth 0, or it won't even know what moves it's allowed to make.");
		
		// Take the MAX of this node's children.
		Node max = children.get(0);
		for(Node n : children) {
			if(max.score() < n.score()) {
				max = n;
			}
		}
		
		// Return the best possible move.
		return max.board;
	}
	
	
	/** Compute the score for this node. Higher is better for the AI.
	 * 
	 * @return - an int representing this node's score.
	 */
	public int score() {
		// If this node has children,
		if(this.children != null && !this.children.isEmpty()) {
			// If we're at a MIN node, take the min of the children.
			if(this.type == MIN) return min(children);
			
			// If we're at a MAX node, take the max of the children.
			else return max(children);
		}
		// If this node has no children, return the score for our board.
		else {
			return board.scoreDif();
		}
	}
	
	/** Compute the MIN from a list.
	 * 
	 * @param l - the list to compute from.
	 * @return - the min of that list.
	 */
	public int min(List<Node> l) {
		int min = Integer.MAX_VALUE;
		for(Node n : l) {
			int s = n.score();
			if(s < min) min = s;
		}
		return min;
	}
	
	/** Compute the MAX from a list.
	 * 
	 * @param l - the list to compute from.
	 * @return - the max of that list.
	 */
	public int max(List<Node> l) {
		int max = Integer.MIN_VALUE;
		for(Node n : l) {
			int s = n.score();
			if(s > max) max = s;
		}
		return max;
	}

	/**
	 * @return - this node's parent.
	 */
	public Node getParent() {
		return parent;
	}
	
	/**
	 * @return - this node's depth.
	 */
	public int getDepth() { 
		return this.depth;
	}
	
	/** An overridden toString method.
	 * 
	 */
	@Override
	public String toString() {
		return board.toString();
	}
}
