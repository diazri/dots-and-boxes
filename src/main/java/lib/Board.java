package lib;

import java.util.*;

public class Board {

	// Dots will be represented by asterisks. Lines will be ASCII blocks, and spaces will be a space.
	public static final char DOT = '*';
	public static final char LINE = 0x2588;
	public static final char SPACE = ' ';
	
	// Defining booleans for turn.
	public static final boolean PLAYER = true;
	public static final boolean AI = false;
	
	// Scores for the player and the AI.
	private int playerScore, AIScore;
	
	// Whose turn is it?
	private final boolean isPlayerTurn;
	
	// The game board.
	private final char[][] board;
	
	/** A constructor. Initializes a new "x by y" board with random box-weights.
	 * 
	 * @param x - the x-dimension of the board.
	 * @param y - the y-dimension of the board.
	 */
	public Board(int x, int y) {
		// Start with the player.
		isPlayerTurn = PLAYER;
		
		// Scores begin at 0.
		playerScore = 0;
		AIScore = 0;
		
		// "Stretch out" the x and y values so we have x*y many BOXES.
		int dx = (2*x) + 1; 
		int dy = (2*y) + 1;
		
		// Set up a new board.
		char[][] temp = new char[dx][dy];
		
		// For each space,
		for(int i = 0; i < dx; i++) {
			for(int j = 0; j < dy; j++) {
				// If the space is even-even, add a dot.
				if( (i % 2 == 0) && (j % 2 == 0) ) temp[i][j] = DOT; 
				
				// If the space is even-odd, add an empty space.
				else if( (i % 2 == 0) || (j % 2 == 0) ) temp[i][j] = SPACE;
				
				// If the space is odd-odd, add a random int.
				else temp[i][j] = ( String.valueOf( Math.round( Math.random() * 10 ) ) ).charAt(0);
			}
		}
		
		// Now that we have our board finalized, save it.
		board = temp;
	}
	
	/** Build a board from a given array.
	 * 
	 * @param b - the array to use.
	 * @param p - the player's score.
	 * @param a - the AI's score.
	 */
	public Board(char[][] b, int p, int a, boolean t) {
		// Import board.
		board = b;
		
		// Set scores.
		playerScore = p;
		AIScore = a;
		
		// Set the turn.
		isPlayerTurn = t;
	}
	
	/** The successor function.
	 * 
	 * @return - a list of legal successor boards.
	 */
	public List<Board> succ() {
		LinkedList<Board> l = new LinkedList<Board>();
		
		// For each space,
		for(int i = 0; i < board.length; i++) {
			for(int j = 0; j < board[0].length; j++) {
				// If the space is blank,
				if(board[i][j] == SPACE) {
					// Add a new successor board with a move made here.
					l.add( move(i, j) );
				}
			}
		}
		
		return l;
	}
	
	/** Verify that a move is legal.
	 * 
	 * @param x - the x-coordinate of the move.
	 * @param y - the y-coordinate of the move.
	 * @return - true if the move is legal, false otherwise.
	 */
	public boolean isLegal(int x, int y) {
		if(x >= board.length || y >= board[0].length) return false;
		else return board[x][y] == SPACE;
	}
	
	/** Make a copy of this board, and fill in the line at x, y.
	 * 
	 * @param x - the x-coordinate of the new line.
	 * @param y - the y-coordinate of the new line.
	 * @return - the board after our move is made.
	 */
	public Board move(int x, int y) {
		// Disallow illegal moves.
		if(!isLegal(x,y)) throw new IllegalStateException("ERROR: You're trying to make an illegal move!");
		
		// Create a successor state which is a copy of our array.
		char[][] temp = new char[board.length][board[0].length];
		for(int n = 0; n < board.length; n++) {
			temp[n] = board[n].clone();
		}
		
		// Replace the space with a line.
		temp[x][y] = LINE;
		
		// Compute the added score, if any.
		int score = tally(temp, x, y);
		
		// Compute the new scores for each player.
		int p = playerScore;
		int a = AIScore;

		if(isPlayerTurn) p += score;
		else a += score;
		
		// Make our board & return it.
		return new Board(temp, p, a, !isPlayerTurn);
	}
	
	/** Compute the score difference.
	 * 
	 * @return - the difference between the AI's score and the player's score.
	 */
	public int scoreDif() {
		return AIScore - playerScore;
	}
	
	/** Compute the score created by a new move.
	 * 
	 * @param b - the board (after the move).
	 * @param x - the x-coordinate of the move.
	 * @param y - the y-coordinate of the move.
	 * @return - the score the move results in.
	 */
	public int tally(char[][] b, int x, int y) {
		int sum = 0;
		
		// For each box this move might have completed,
		// add the score for that box, if any.
		sum += points(b, x-1, y);
		sum += points(b, x+1, y);
		sum += points(b, x, y-1);
		sum += points(b, x, y+1);
		
		return sum;
	}
	
	/** Get the score of a given cell.
	 * 
	 * @param b - the board to operate on.
	 * @param x - the x-coordinate of the cell.
	 * @param y - the y-coordinate of the cell.
	 * @return
	 */
	public int points(char[][] b, int x, int y) {
		// If we're looking out-of-bounds, return 0 (the cell can't be an int).
		if (x < 1 || x > board.length - 2) return 0;
		if (y < 1 || y > board[0].length - 2) return 0;
		
		// If the cell is not surrounded by lines, return 0.
		if	   (b[x+1][y] != LINE) return 0;
		else if(b[x-1][y] != LINE) return 0;
		else if(b[x][y+1] != LINE) return 0;
		else if(b[x][y-1] != LINE) return 0;
		
		// If the box is complete, get the point value of the cell.
		else					   return val(b, x, y);
	}
	
	/** Get the value of a given cell.
	 * 
	 * @param x - the x-coordinate of the cell.
	 * @param y - the y-coordinate of the cell.
	 * @return - the point-value of the cell if it's a box, 0 otherwise.
	 */
	public int val(char[][]b, int x, int y) {
		if ( (x % 2 == 0) || (y % 2 == 0) ) return 0;
		else {
			String s = "" + b[x][y];
			return Integer.valueOf(s);
		}
	}
	
	/** An overridden equals method.
	 * 
	 */
	@Override
	public boolean equals(Object o) {
		// If o is not a board, return false.
		if(o.getClass() != this.getClass()) return false;
		
		Board b = (Board) o;
		
		// If b doesn't have the same dimensions as this board, return false.
		if(b.board.length != this.board.length) return false;
		if(b.board[0].length != this.board[0].length) return false;
		
		// If any of the cells mismatch, return false.
		for(int i = 0; i < board.length; i++) {
			for(int j = 0; j < board[0].length; j++) {
				if(this.board[i][j] != b.board[i][j]) return false;
			}
		}
		
		// Everything's equal, so return true.
		return true;
	}
	
	/** An overridden toString method.
	 * 
	 */
	@Override
	public String toString() {
		String s = "";
		
		// The board itself.
		for(int i = 0; i < board.length; i++) {
			for(int j = 0; j < board[0].length; j++) {
				s += this.board[i][j];
			}
			s += "\n";
		}
		
		// The scores.
		s += "Player score: " + playerScore + "\n";
		s += "AI score: " + AIScore + "\n";
		
		// Whose turn is it?
		if(isPlayerTurn) s += "Player's turn.";
		else s += "AI's turn.";
		
		return s;
	}
}
