package libtests;

import org.junit.*;

import static org.junit.Assert.assertEquals;

import java.util.*;

import lib.Board;

public class BoardTests {
	
	public static final char L = 0x2588;
	private static final char[][] SMALL = { {'*', L, '*'}, {' ', '5', ' '}, {'*', ' ', '*'} };
	
	private Board sm, lg;
	
	@Before
	public void setup() {
		sm = new Board(SMALL, 0, 0, true);
		lg = new Board(5, 5);
		
		System.out.println("Small board:\n" + sm + "\n");
		System.out.println("Large board:\n" + lg + "\n");
	}
	
	@Test
	public void testSucc() {
		List<Board> succs = sm.succ();
		for(Board b : succs) {
			System.out.println(b);
			System.out.println();
		}
		assertEquals(3, succs.size());
		
		succs = lg.succ();
		assertEquals(60, succs.size());
	}
}
