package libtests;

import static org.junit.Assert.assertEquals;

import org.junit.*;
import lib.*;
import search.*;

public class SearchTests {

	public static final char L = 0x2588;
	
	private static final char[][] START = { {'*', ' ', '*', ' ', '*'},
											{ L,  '5',  L,  '1', ' '},
											{'*',  L,  '*', ' ',  '*'} };
	
	private static final char[][] MOVE =  { {'*',  L,  '*', ' ', '*'},
											{ L,  '5',  L,  '1', ' '},
											{'*',  L,  '*', ' ', '*'} };
	
	private Board start, fin;
	
	@Before
	public void before() {
		start = new Board(START, 0, 0, Board.AI);
		fin = new Board(MOVE, 0, 5, Board.PLAYER);
	}
	
	@Test
	public void searchTest() {
		Board res = MinMax.move(start, 1);
		assertEquals(res, fin);
		
		res = MinMax.move(start, 3);
		assertEquals(res, fin);
		
		res = MinMax.move(start, 100);
		assertEquals(res, fin);
	}
}
